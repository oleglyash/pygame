#-*-coding:utf-8-*-
# Выполнил: Марцынковская Надежда Павловна
# Руководитель: Ляш Олег Иванович
'''
Игра "Батискаф".
Игроку необходимо собирать сундуки с золотом.
'''
####
try:
    import random
    import pygame
    from pygame.locals import *
except ImportError, err:
    print "Не могу загрузить модуль %s" % (err)
    sys.exit(2)

path='data/'
#Словарь, хранящий информацию об игре (размер экрана/окна по умолчанию, объект-ЭКРАН, путь к ресурсам игр, объект-шрифт)
game_info={'size':[800,600],
           'screen':None,
           'font':None,
           'level':1,
           'clock':None,
           'fon':None,
           'buf':None,
           'mouse_x':None,
           'mouse_y':None,
           'font':None,
           'speed':5,
           'score_cur':0,
           'score_add1':10,
           'score_add2':50,
           'score_next':100,
           'score_step_next':100,
           'life':3}
#Словарь, хранящий состояние игры (игра запущена, первый запуск)           
game_state={'done':False,
            'menu':True,
            'pause':False,
            'about':False,
            'records':False,
            'gameover':False}
                    
#Словарь, хранящий состояние управляющих клавиш
game_keys={'left':False,
           'up':False,
           'right':True,
           'down':False,
           'fire':False,
           'mousedown':False}
#Словарь для изображений
images={}
#Словарь для пунктов меню
menu={}
#Список для акул
sharks=[]
sharks_count=1
#Список для алмазов/ракушек/сундуков
bonus=[]
bonus_count=3
#Список для декора
decor=[]
decor_count=6
#Список для пузырьков
balloons=[]
balloons_count=10
#Словарь для батискафа
bathyscaphe={}
boom_timeout=10

def load_images():
    global game_info,game_state,game_keys,images,menu,sharks,sharks_count,bonus,balloons,bathyscaphe
    #Загрузка изображений в cловарь
    images={'fon1':pygame.image.load(path+'img/fon1.png').convert(),
            'fon2':pygame.image.load(path+'img/fon2.png').convert(),
            'fon3':pygame.image.load(path+'img/fon3.png').convert(),
            'fon4':pygame.image.load(path+'img/fon4.png').convert(),
            'fon5':pygame.image.load(path+'img/fon5.png').convert(),
            'fon6':pygame.image.load(path+'img/fon6.png').convert(),
            'fon7':pygame.image.load(path+'img/fon7.png').convert(),
            'fon8':pygame.image.load(path+'img/fon8.png').convert(),
            'fon9':pygame.image.load(path+'img/fon9.png').convert(),
            'fon10':pygame.image.load(path+'img/fon10.png').convert(),
            'balloon1':pygame.image.load(path+'img/balloon1.png').convert_alpha(),
            'balloon2':pygame.image.load(path+'img/balloon2.png').convert_alpha(),
            'shark1':pygame.image.load(path+"img/shark1.png").convert_alpha(),
            'shark2':pygame.image.load(path+"img/shark2.png").convert_alpha(),
            'shark3':pygame.image.load(path+"img/shark3.png").convert_alpha(),
            'boom':pygame.image.load(path+"img/boom.png").convert(),
            'bathyscaphe':pygame.image.load(path+'img/bathyscaphe.png').convert_alpha(),
            'item1':pygame.image.load(path+"img/item1.png").convert_alpha(),
            'item2':pygame.image.load(path+"img/item2.png").convert_alpha(),
            'inform':pygame.image.load(path+"img/inform.png").convert(),
            'bonus1':pygame.image.load(path+"img/almaz.png").convert_alpha(),
            'bonus2':pygame.image.load(path+"img/shell1.png").convert_alpha(),
            'bonus3':pygame.image.load(path+"img/shell2.png").convert_alpha(),
            'decor1':pygame.image.load(path+"img/decor1.png").convert_alpha(),
            'decor2':pygame.image.load(path+"img/decor2.png").convert_alpha(),
            'decor3':pygame.image.load(path+"img/decor3.png").convert_alpha(),
            'decor4':pygame.image.load(path+"img/decor4.png").convert_alpha(),
            'decor5':pygame.image.load(path+"img/decor5.png").convert_alpha(),
            'decor6':pygame.image.load(path+"img/decor6.png").convert_alpha(),
            'gameover':pygame.image.load(path+'img/gameover.png').convert(),
            }
    images['boom'].set_colorkey((0,0,0))
    images['boom'].set_alpha(255)
    #Масштабирование фоновой картинки под размер окна
    game_info['fon']=pygame.transform.scale(images['fon1'],(game_info['screen'].get_width(),game_info['screen'].get_height()))
    #Загрузка фоновой картинки
    game_info['buf']=pygame.image.load("data/img/fon1.png").convert()
# <<< def load_images()

def init():
    '''
    Инициализация pygame и задание некоторых начальных параметров
    '''
    global game_info,game_state,game_keys,images,menu,sharks,sharks_count,bonus,balloons,bathyscaphe
    pygame.init()
    game_info['screen']=pygame.display.set_mode(game_info['size'])
    pygame.display.set_caption("Game-Bathyscaphe")
    #Установка иконки для окна
    icon=pygame.image.load(path+"img/bathyscaphe.png").convert_alpha()
    icon=pygame.transform.scale(icon,(32,32))
    pygame.display.set_icon(icon)
    #Включаем указатель мыши
    pygame.mouse.set_visible(1)
    #Инициализация шрифтов
    pygame.font.init()
    game_info['font']=pygame.font.Font(None,30) #Размер шрифта
    game_info['clock']=pygame.time.Clock()
    
    load_images()
    
    #Меню
    menu={'x':game_info['screen'].get_width()//2,
    'y':game_info['screen'].get_height()//2,
    'itemcolor':(128,0,64),
    'itemselcolor':(255,0,128),
    'count':4,
    'menu_shift':60,
    'item0':u'Старт',
    'item0_img':None,
    'item1':u'Об игре',
    'item1_img':None,
    'item2':u'Рекорды',
    'item2_img':None,
    'item3':u'Выход',
    'item3_img':None}
    
    #Загрузка звуков
    pygame.mixer.music.load(path+'sound/music1.mp3')
    pygame.mixer.music.play()
    #Установка параметров батискафа
    bathyscaphe={'x':0,
                 'y':game_info['screen'].get_height()//2,
                 'speed':5,
                 'boom':0}
    #Установка начальных занчений для акулы
    for i in range(sharks_count):
        a={'x':game_info['screen'].get_width(),
        'y':random.randint(1,game_info['screen'].get_height()-images['shark1'].get_height()),
        'img':images['shark'+str(random.randint(1,3))],
        'speed':random.randint(5,10)
        }
        sharks.append(a)

    #Установка начальных занчений для декора
    for i in range(decor_count):
        a={'x':game_info['screen'].get_width()+random.randint(10,50),
        'y':game_info['screen'].get_height()-images['decor4'].get_height(),
        'img':images['decor'+str(random.randint(1,6))],
        'speed':random.randint(5,10)
        }
        decor.append(a)

    #Установка начальных занчений для призов/бонусов
    for i in range(bonus_count):
        a={'x':game_info['screen'].get_width()+random.randint(10,50),
        'y':game_info['screen'].get_height()-images['bonus1'].get_height(),
        'img':images['bonus'+str(random.randint(1,3))],
        'speed':random.randint(5,10)
        }
        bonus.append(a)

    #Установка начальных занчений для пузырьков
    for i in range(balloons_count):
        a={'x':random.randint(1,game_info['screen'].get_width()-images['balloon1'].get_width()),
        'y':game_info['screen'].get_height(),
        'img':images['balloon'+str(random.randint(1,2))],
        'speed':random.randint(5,10)
        }
        balloons.append(a)
# <<< def init()

def events():
    '''
    Обработка игровых событий 
    '''
    global game_info,game_state,game_keys,images,menu,sharks,sharks_count,bonus,balloons,bathyscaphe
    #Обработка событий
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            game_state['done']=True
        #Если пользователь нажал на кнопку
        if event.type==pygame.KEYDOWN:
            #Если нажата кнопка выхода
            if game_state['gameover']==True:
                game_state['gameover']==False
                game_state['menu']=True
            if event.key==pygame.K_ESCAPE:
                if game_state['about']==True:
                    game_state['menu']=True
                    game_state['about']=False
                elif game_state['menu']==True:
                    game_state['done']=True
                elif game_state['gameover']==True:
                    game_state['menu']=True
                    game_state['gameover']=False   
                elif game_state['records']==True:
                    game_state['records']=False
                    game_state['menu']=True
                elif game_state['menu']==False:
                    game_state['menu']=True                    
            #Пауза
            if event.key==pygame.K_SPACE:
                game_state['pause']=not(game_state['pause'])
                
                #game_info['level']+=1
                #game_info['buf']=images['fon'+str(game_info['level'])]
                #game_info['fon']=images['fon'+str(game_info['level'])]
            #Если нажали кнопку вверх
            elif event.key==pygame.K_UP:
                game_keys['up']=True
            elif event.key==pygame.K_DOWN:
                game_keys['down']=True
        
        #Если пользователь отпустил кнопку
        if event.type==pygame.KEYUP:
            if event.key==pygame.K_UP:
                game_keys['up']=False
            elif event.key==pygame.K_DOWN:
                game_keys['down']=False
                
        if event.type==pygame.MOUSEBUTTONDOWN:
            if event.button==1:
                game_keys['mousedown']=True
        if event.type==pygame.MOUSEBUTTONUP:
            if event.button==1:
                game_keys['mousedown']=False
        
        #Получем координаты указателя мыши
        pos=pygame.mouse.get_pos()
        game_info['mouse_x']=pos[0]
        game_info['mouse_y']=pos[1] 
# <<< def events()
        
def print_menu():
    '''Функция, которая выводит пункты меню на экран
    '''
    global game_info,game_state,game_keys,images,menu,sharks,sharks_count,bonus,balloons,bathyscaphe
    
    if menu['item1_img']==None:
        #Вычисляем координаты для вывода пункта меню 
        menu['x']=game_info['screen'].get_width()//2
        menu['y']=game_info['screen'].get_height()//2-images['item1'].get_height()//2-menu['menu_shift']*menu['count']//2        
    #генерируем изображение с текстом пункта меню
    for i in range(menu['count']):
        menu['item'+str(i)+'_img']=game_info['font'].render(menu['item'+str(i)],False,menu['itemcolor'])
        
        #Если указатель мыши попал в пункт меню, то меняем цвет надписи
        edge_left=game_info['mouse_x']>menu['x']-images['item1'].get_width()//2
        edge_right=game_info['mouse_x']<menu['x']-images['item1'].get_width()//2+images['item1'].get_width()
        edge_top=game_info['mouse_y']>menu['y']+menu['menu_shift']*i
        edge_bottom=game_info['mouse_y']<menu['y']+menu['menu_shift']*i+images['item1'].get_height()
        
        if edge_left and edge_right and edge_top and edge_bottom:
               menu['item'+str(i)+'_img']=game_info['font'].render(menu['item'+str(i)],False,menu['itemselcolor'])
               #Если нажата кнопка мыши, то меняем флаги соответствующих пунктов меню
               if game_keys['mousedown']==True:
                   if i==0:
                       game_state['menu']=False
                       game_state['pause']=False
                       game_state['about']=False
                       game_state['records']=False
                       game_state['gameover']=False
                       #Установка начальных параметров игры (жизни, очки, уровень)
                       game_info['level']=1
                       game_info['score_cur']=0
                       game_info['score_add1']=10
                       game_info['score_add2']=50
                       game_info['score_next']=100
                       game_info['score_step_next']=100
                       game_info['life']=3
                   if i==1:
                       game_state['menu']=False
                       game_state['pause']=False
                       game_state['about']=True
                       game_state['records']=False
                       game_state['gameover']=False                        
                   if i==2:
                       game_state['menu']=False
                       game_state['pause']=False
                       game_state['about']=False
                       game_state['records']=True
                       game_state['gameover']=False                        
                   if i==3:
                       game_state['done']=True
        #game_keys['mousedown']=False

    #Выводим пункты меню красиво по центру экрана
    for i in range(menu['count']):  
        #Фоновая картинка
        game_info['screen'].blit(images['item1'],[menu['x']-images['item1'].get_width()//2,menu['y']+menu['menu_shift']*i])    
        #Текст пунктов меню
        game_info['screen'].blit(menu['item'+str(i)+'_img'],
        [menu['x']-menu['item'+str(i)+'_img'].get_width()//2,
        menu['y']+menu['menu_shift']*i+menu['item'+str(i)+'_img'].get_height()//2])
# <<< def print_menu()

def print_about():
    '''Вывод информации о программе
    '''
    game_info['screen'].blit(images['inform'],[0,0])
# <<< def print_about()    

def print_gameover():
    '''Вывод информации о программе
    '''
    game_info['screen'].blit(images['gameover'],[0,0])
# <<< def print_about()    
    
def print_records():
    '''Вывод таблицы рекордов
    '''
    print('Таблица рекордов')
# <<< def print_records()    
    
def print_game():
    '''Вывод игровых объектов и т.п.
    '''
    if game_state['pause']==True:
        print('Игра на паузе')
        text=game_info['font'].render('P A U S E',False,menu['itemselcolor'])
        game_info['screen'].blit(text,[400,300])
    else:
        #Обработка нажатий на клавиши
        if (game_keys['up']==True) and (bathyscaphe['boom']<=0):
            bathyscaphe['y']-=bathyscaphe['speed']
        if (game_keys['down']==True) and (bathyscaphe['boom']<=0):
            bathyscaphe['y']+=bathyscaphe['speed']
        
        #Обработка списка с информацией об акулах
        for i in sharks:
            i['x']-=i['speed']
            #Если акула уехала за левую границу экрана
            if i['x']+i['img'].get_width()<0:
                #то отправляем ее за правую границу
                i['x']=game_info['screen'].get_width()
                #и генерируем случайную координату по оси OY
                i['y']=int(random.randint(1,game_info['screen'].get_height()-images['shark1'].get_height()))
                i['img']=images['shark'+str(random.randint(1,3))]
                i['speed']=random.randint(5,10)

            #Анализ столкновения с акулами
            '''  Старый вариант
            edge_left=(bathyscaphe['x']>i['x']) and (bathyscaphe['x']<i['x']+i['img'].get_width())
            edge_right=(bathyscaphe['x']+images['bathyscaphe'].get_width()>i['x']) and (bathyscaphe['x']+images['bathyscaphe'].get_width()<i['x']+i['img'].get_width())
            edge_top=(bathyscaphe['y']>i['y']) and (bathyscaphe['y']<i['y']+i['img'].get_height())
            edge_bottom=(bathyscaphe['y']+images['bathyscaphe'].get_height()>i['y']) and (bathyscaphe['y']+images['bathyscaphe'].get_height()<i['x']+i['img'].get_height())            
            if (edge_left or edge_right) and (edge_top or edge_bottom):
                bathyscaphe['boom']=boom_timeout
                i['x']=game_info['screen'].get_width()
                i['y']=int(random.randint(1,game_info['screen'].get_height()-images['shark1'].get_height()))
                i['img']=images['shark'+str(random.randint(1,3))]
                i['speed']=random.randint(5,10)  
                game_info['life']-=1
            '''
            #Новый вариант
            #Прямоугольная область в которой находится батискаф
            Rect_batiskaf=Rect(bathyscaphe['x'],
                               bathyscaphe['y'],
                               images['bathyscaphe'].get_width(),
                               images['bathyscaphe'].get_height())            
            #Прямоугольная область в которой находится акула
            Rect_Shark=Rect(i['x'],
                            i['y'],
                            i['img'].get_width(),
                            i['img'].get_height())
            #Если прямоугольные области пересеклись, то
            if (bool(Rect_batiskaf.colliderect(Rect_Shark))==True) and (i['x']>images['bathyscaphe'].get_width()/2):
                #считаем, что акула столкнулась с батискафом
                bathyscaphe['boom']=boom_timeout
                i['x']=game_info['screen'].get_width()
                i['y']=int(random.randint(1,game_info['screen'].get_height()-images['shark1'].get_height()))
                i['img']=images['shark'+str(random.randint(1,3))]
                i['speed']=random.randint(5,10)  
                game_info['life']-=1

            #Выводим акулу
            game_info['screen'].blit(i['img'],[i['x'],i['y']])

            if game_info['life']<=0:
                game_state['gameover']=True                
        # <<< конец обработки списка с информацией об акулах
        
        #Обработка списка с информацией о бонусах/призах
        for i in bonus:
            i['x']-=i['speed']
            #Если акула уехала за левую границу экрана
            if i['x']+i['img'].get_width()<0:
                #то отправляем ее за правую границу
                i['x']=game_info['screen'].get_width()
                #и генерируем случайную координату по оси OY
                i['y']=int(random.randint(1,game_info['screen'].get_height()-images['bonus1'].get_height()))
                i['img']=images['bonus'+str(random.randint(1,3))]
                i['speed']=random.randint(5,10)
            #Прямоугольная область в которой находится батискаф
            Rect_batiskaf=Rect(bathyscaphe['x'],
                               bathyscaphe['y'],
                               images['bathyscaphe'].get_width(),
                               images['bathyscaphe'].get_height())            
            #Прямоугольная область в которой находится акула
            Rect_bonus=Rect(i['x'],
                            i['y'],
                            i['img'].get_width(),
                            i['img'].get_height())
            #Если прямоугольные области пересеклись, то
            if bool(Rect_batiskaf.colliderect(Rect_bonus))==True:
                #считаем, что батискаф столкнулся с призом
                i['x']=game_info['screen'].get_width()
                i['y']=int(random.randint(1,game_info['screen'].get_height()-images['bonus1'].get_height()))
                i['img']=images['bonus'+str(random.randint(1,3))]
                i['speed']=random.randint(5,10)  
                game_info['score_cur']+=game_info['score_add1']
                
                #Если заработали уже много очков, то надо увеличить уровень, кол-во врагов и т.п.
                if game_info['score_cur']>=game_info['score_next']:
                    #Увеличиваем номер уровня
                    game_info['level']+=1
                    #Активируем новое изображение для нового уровня                    
                    if game_info['level']>10:
                        game_info['fon']=pygame.transform.scale(images['fon10'],(game_info['screen'].get_width(),game_info['screen'].get_height()))
                    else:
                        game_info['fon']=pygame.transform.scale(images['fon'+str(game_info['level'])],(game_info['screen'].get_width(),game_info['screen'].get_height()))
                    #Сдвигаем границу набранных очков для перехода на новый уровень
                    game_info['score_next']=game_info['level']*2*game_info['score_step_next']
                    
            #Выводим бонусы
            game_info['screen'].blit(i['img'],[i['x'],i['y']])
        # <<< конец обработки списка с информацией о бонусах/призах
        
        if game_info['life']<=0:
            game_state['gameover']=True                
            
        if bathyscaphe['boom']>0:
            bathyscaphe['boom']-=1
            #Вывод взрыва на месте батискафа
            game_info['screen'].blit(images['boom'],[bathyscaphe['x'],bathyscaphe['y']])
        else:
            #Вывод батискафа
            game_info['screen'].blit(images['bathyscaphe'],[bathyscaphe['x'],bathyscaphe['y']])              
# <<< def print_game()        
        
def print_balloons():
    '''Вывод пузырьков на экран
    '''
    for i in balloons:
        i['y']-=i['speed']
        if i['y']+i['img'].get_height()<0:
            i['y']=game_info['screen'].get_height()
            i['x']=int(random.randint(1,game_info['screen'].get_width()-images['balloon1'].get_width()))
            i['img']=images['balloon'+str(random.randint(1,2))]
            i['speed']=random.randint(5,10)
        game_info['screen'].blit(i['img'],[i['x'],i['y']])              
# <<< def print_balloons()        

def print_decor():
    '''Вывод декора
    '''
    for i in decor:
        i['x']-=i['speed']
        if i['x']+i['img'].get_width()<0:
            r=random.randint(1,6)
            i['img']=images['decor'+str(r)]
            i['x']=game_info['screen'].get_width()+random.randint(10,30)
            if r<5:
                i['y']=game_info['screen'].get_height()-i['img'].get_height()
            else:
                i['y']=int(random.randint(1,game_info['screen'].get_height()-i['img'].get_height()))                
            i['speed']=random.randint(5,10)
        game_info['screen'].blit(i['img'],[i['x'],i['y']])   
# <<< def print_decor()        

def print_geme_info():
    '''Вывод информации
    '''
    global game_info,game_state,game_keys,images,menu,sharks,sharks_count,bonus,balloons,bathyscaphe
    tmp=game_info['font'].render(u'Жизни : '+str(game_info['life']),False,menu['itemcolor'])    
    game_info['screen'].blit(tmp,[10,10])
    
    tmp=game_info['font'].render(u'Очки : '+str(game_info['score_cur']),False,menu['itemcolor'])    
    game_info['screen'].blit(tmp,[10,30])
    
    tmp=game_info['font'].render(u'Очки next : '+str(game_info['score_next']),False,menu['itemcolor'])    
    game_info['screen'].blit(tmp,[10,50])    
    
    tmp=game_info['font'].render(u'Уровень : '+str(game_info['level']),False,menu['itemcolor'])    
    game_info['screen'].blit(tmp,[10,70])    
# <<< def print_game_info()       
    
def main():
    global game_info,game_state,game_keys,images,menu,sharks,sharks_count,bonus,balloons,bathyscaphe
    #Вызов функции инициализации игровых параметров
    init()
    while game_state['done']==False:
        events()        
        #Прокрутка1 справа налево
        game_info['buf'].blit(game_info['fon'],[0,0],[game_info['speed'],0,game_info['fon'].get_width(),game_info['fon'].get_height()])
        game_info['buf'].blit(game_info['fon'],
                 [game_info['fon'].get_width()-game_info['speed'],0],
                 [0,0,game_info['speed'],game_info['fon'].get_height()])
        #Рисуем фон1
        game_info['fon'].blit(game_info['buf'],[0,0])
        game_info['screen'].blit(game_info['fon'],[0,0])
        
        #Выводим пузырьки
        print_balloons()
        #Выводим декор
        print_decor()
                
        if game_state['menu']==True:
            #Выводим меню
            print_menu()
        if game_state['menu']==False and game_state['about']==True:
            #Выводим информацию о программе
            print_about()
        if game_state['menu']==False and game_state['records']==True:
            #Выводим таблицу рекордов
            print_records() 
        if game_state['menu']==False and game_state['records']==False and  game_state['about']==False:
            #Выводим игру :-)
            print_game()
            print_geme_info()   
        if game_state['menu']==False and game_state['gameover']==True:
            #Выводим сообщение о том, что игра закончилась
            print_gameover()
        
        pygame.display.flip()
        game_info['clock'].tick(20)

    pygame.quit()
# <<< def main()    

main()
