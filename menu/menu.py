#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Компьютерная игра "StarWars".
Игорку необходимо подбивать всех врагов, которые появляются сверху экрана.
'''
####

# Импортировать библиотеку под названием 'pygame'
import pygame
import random


def boombox():
    '''звук выстрела
    '''
    file ='data/sound/gun2.wav'
    sound = pygame.mixer.Sound(file)
    channel = sound.play()
    
def main():
    ###Инициализация и загрузка данных
    path="data/"
    # Инициализировать игровой движок
    pygame.init()
    # Задать ширину и высоту экрана
    size=[800,600]
    screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("StarWars")
    #Отключение отображения указателя мыши
    pygame.mouse.set_visible(1)
    #Инициализация шрифтов
    pygame.font.init()
    font1 = pygame.font.Font(None, 80) #для вывода крупных надписей

    
    # Используется для контроля частоты обновления экрана
    clock=pygame.time.Clock()    
    
    #Загрузка изображений
    images={'fon':pygame.image.load(path+"img/fon.jpg").convert(),
            'item1':pygame.image.load(path+"img/item1.png").convert_alpha(),
            'item2':pygame.image.load(path+"img/item2.png").convert_alpha(),
            }
    #Масштабируем фоновую картинку под текущий размер окна
    images['fon']=pygame.transform.scale(images['fon'],(screen.get_width(),screen.get_height()))
    #Загружаем фоновую картинку в буфер. Буфер используем для прокрутки фона
    buf=pygame.image.load("data/img/fon.jpg").convert()


    item1={'x':screen.get_width()//2-images['item1'].get_width()//2,
           'y':screen.get_height()//2-images['item1'].get_height()//2
        }
    item2={'x':item1['x'],
           'y':item1['y']+images['item1'].get_height()+5
        }    
    #Загружаем звуки
    pygame.mixer.music.load(path+'sound/DestroyHead_-_Space_Things.mp3')
    pygame.mixer.music.play()

    boombox = pygame.mixer.Sound(path+'sound/gun2.wav')

   
    #Задаем стартовые параметры игровым переменным
    max_speed=30 #Максимальная скорость
    speed=1 #Текущая скорость

    #Переменные для фиксации нажатых клавиш
    #по умолчанию ничего не нажато
    keyup=False
    keydown=False
    keyspace=False


    done=False
    while done==False:
       # Обработка событий
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                done=True
            #Если пользователь нажал на кнопку
            if event.type == pygame.KEYDOWN:
                #Если нажата кнопка выхода (Escape)
                if event.key == pygame.K_ESCAPE:
                    done=True # то установить флаг для выхода

        #Обработка логики игры
        #Получаем координаты мыши
        pos = pygame.mouse.get_pos()
        x=pos[0]
        y=pos[1]
        #Вывод на экран
            
        #Прокрутка справа налево
        buf.blit(images['fon'],[0,0],[speed,0,images['fon'].get_width(),images['fon'].get_height()])
        buf.blit(images['fon'],[images['fon'].get_width()-speed,0],[0,0,speed,images['fon'].get_height()])

        #Рисуем фон
        images['fon'].blit(buf,[0,0])

        screen.blit(images['fon'],[0,0])
        
        if x>item1['x'] and x<item1['x']+images['item1'].get_width() and y>item1['y'] and y<item1['y']+images['item1'].get_height():
            screen.blit(images['item2'],[item1['x'],item1['y']])
        else:
            screen.blit(images['item1'],[item1['x'],item1['y']])

        if x>item2['x'] and x<item2['x']+images['item1'].get_width() and y>item2['y'] and y<item2['y']+images['item1'].get_height():
            screen.blit(images['item2'],[item2['x'],item2['y']])
        else:
            screen.blit(images['item1'],[item2['x'],item2['y']])            

        text = font1.render("N E W ",False,(125,125,125))
        w=item1['x']+images['item1'].get_width()//2-text.get_width()//2
        h=item1['y']+images['item1'].get_height()//2-text.get_height()//2        
        screen.blit(text, [w,h])    
        
        text = font1.render("Q U I T ",True,(125,125,125))
        w=item2['x']+images['item2'].get_width()//2-text.get_width()//2
        h=item2['y']+images['item2'].get_height()//2-text.get_height()//2        
        screen.blit(text, [w,h])                

            
        pygame.display.flip()
        clock.tick(20)

    pygame.quit()

main()
