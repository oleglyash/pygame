#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Компьютерная игра "Шарики". Игорку необходимо проколоть как можно больше шариков.
После удачного прокалывания шарика начисляются баллы, после того как шарик улетел
у игрока отнимается попытка
'''
####

# Импортировать библиотеку под названием 'pygame'
import pygame, random
      
def main():
    path='data/'
    # Инициализировать игровой движок
    pygame.init()
    # Определение некоторых цветов
    black = ( 0, 0, 0)
    white = ( 255, 255, 255)
    # Задать ширину и высоту экрана
    #pygame.display.list_modes()[0][0] - это максимальное разрещение экрана по оси OX
    #pygame.display.list_modes()[0][1] - это максимальное разрещение экрана по оси OY
    size=[pygame.display.list_modes()[0][0],pygame.display.list_modes()[0][1]]
    #Собственно создание экрана с заданными размерами
    screen=pygame.display.set_mode(size, pygame.FULLSCREEN)
##    screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("Balloons")

    #Загружаем музыку
    pygame.mixer.music.load(path+'snd/2Inventions_-_Johaness_Gilther_-_Subcarpathia__Original_Mix_.mp3')
    # Начинаем проигрывать музыку,
    pygame.mixer.music.play()
    
    #Загружаем взрыв
    soundBoom1=pygame.mixer.Sound(path+'snd/gun1.wav')
    soundBoom1.set_volume(0.1)
    soundBoom2=pygame.mixer.Sound(path+'snd/gun2.wav')
    soundBoom2.set_volume(0.3)
    soundLevel=pygame.mixer.Sound(path+'snd/iron2.wav')
    
    #Загрузка изображений
    #картинки загружаются в словарь
    images={'fon':pygame.image.load(path+"img/fon1.jpg").convert(),
            'boom1':pygame.image.load(path+"img/boom.png").convert_alpha(),
            'boom2':pygame.image.load(path+"img/boom.gif").convert_alpha(),
            'balloon1':pygame.image.load(path+"img/balloon1.png").convert_alpha(),
            'balloon2':pygame.image.load(path+"img/balloon2.png").convert_alpha(),
            'balloon3':pygame.image.load(path+"img/balloon3.png").convert_alpha(),
            'balloon4':pygame.image.load(path+"img/balloon4.png").convert_alpha(),
            'balloon5':pygame.image.load(path+"img/balloon5.png").convert_alpha(),
            'balloon6':pygame.image.load(path+"img/balloon5.png").convert_alpha(),
            'cursor':pygame.image.load(path+"img/cursor.png").convert_alpha(),
            'paper1':pygame.image.load(path+"img/paper1.png").convert()}
    images['paper1'].set_colorkey((0,0,0))

    zzz=images['paper1']
    
    images['paper1'].set_alpha(100)

##    images['balloon1'].set_colorkey((255,0,0))
##    images['balloon1'].set_alpha(150)
##
##    images['balloon2'].set_colorkey((0,0,0))
##    images['balloon2'].set_alpha(100)
##
##    images['balloon3'].set_colorkey((0,0,0))
##    images['balloon3'].set_alpha(100)
##
##    images['balloon4'].set_colorkey((0,0,0))
##    images['balloon4'].set_alpha(100)        
    
    #Задаем стартовые параметры игровым переменным
    level=1 #Уровень
    level_change=False #Флаг, показывающий смену уровня
    level_change_time=0 #Счетчик для задержки надписи о смене уровня на экране
    score_cur=0 #текущие очки
    score_add=10 #сколько добавлять за подбитый шарик
    score_next=100 #сколько очков надо заработать для перехода на следующий уровень
    score_step_next=100 #на сколько следует увеличить планку перехода на следующий уровень
    life=5 #кол-во жизней
    count=3 #кол-во шариков, которые видны на экране
    speed=15 #скокрость движения шарикров
    #Создаем список с информацией о шариках
    #координаты, картинка, скорость.
    #img,x,y,speed
    balloons=[]
    for i in range(count):
        balloons.append([random.randint(1,6),
                        random.randrange(size[0]-images['balloon1'].get_width()-30),
                        random.randrange(size[1]),
                        random.randint(speed//1.5,speed)]
                        )
        
    #Оставаться в цикле, пока пользователь не нажмёт на кнопку закрытия окна
    done=False # Переменная для основного игрового цикла (Флаг игры)
    pause=False # переменная для постановки на паузу (Флаг паузы)
    menu=True
    earthquake=0 # Дрожжание экрана
    earthquake_time=0 #Время в течении которого экран дрожжит
    # Используется для контроля частоты обновления экрана
    clock=pygame.time.Clock()   
    pygame.font.init()

    font = pygame.font.Font(None, 25) #для вывода игровой информации
    font2 = pygame.font.Font(None, 80) #для вывода крупных надписей
    #Прячем курсор
    pygame.mouse.set_visible(0)    
    # -------- Основной цикл программы -----------    
    while done==False:
        shoot=False # перменная для выстрела (Флаг выстрела)
        #Получаем координаты мыши
        pos = pygame.mouse.get_pos()
        x=pos[0]
        y=pos[1]
        #Задаем картинку взрыва (это обычный взрыв без попадания по шарику)
        boom=images['boom1']
        # ОБРАБОТКА ВСЕХ СОБЫТИЙ ДОЛЖНА БЫТЬ ПОД ЭТИМ КОММЕНТАРИЕМ
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
##                    done=True
                    menu=True
                if event.key == pygame.K_SPACE:
                    pause=not(pause)#ставим игру на паузу
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    shoot=True
        # ОБРАБОТКА ВСЕХ СОБЫТИЙ ДОЛЖНА НАХОДИТЬСЯ НАД ЭТИМ КОММЕНТАРИЕМ
        #Если пользователь ранее попал по шарику, то
        if earthquake_time>0:
            earthquake=-earthquake #Меняем направление сдвига экрана
            earthquake_time-=1#Уменьшаем время дрожжания экрана
        if menu==True:
            #Отображаем пользовательский курсор
            screen.blit(images['fon'],[0,0])
            screen.blit(images['cursor'],[x,y])
            menucolor=(0,0,255)
            menucolor_sel=(255,0,0)
            text = font2.render("N E W",False,menucolor,(0,0,0)).convert()            
            edge_left=x>size[0]//2-text.get_width()//2
            edge_right=x<size[0]//2-text.get_width()//2+text.get_width()
            edge_top=y>size[1]//2-text.get_height()//2-100
            edge_bottom=y<size[1]//2-text.get_height()//2-100+text.get_height()
            if edge_left and edge_right and edge_top and edge_bottom:
                text = font2.render("N E W",False,menucolor_sel,(0,0,0)).convert()
                if shoot: menu=not(menu)
            text.set_colorkey((0,0,0))
            text.set_alpha(100)
            screen.blit(text, [size[0]//2-text.get_width()//2,size[1]//2-text.get_height()//2-100])

            text = font2.render("A B O U T",False,menucolor,(0,0,0)).convert()
            edge_left=x>size[0]//2-text.get_width()//2
            edge_right=x<size[0]//2-text.get_width()//2+text.get_width()
            edge_top=y>size[1]//2-text.get_height()//2
            edge_bottom=y<size[1]//2-text.get_height()//2+text.get_height()
            if edge_left and edge_right and edge_top and edge_bottom:
                text = font2.render("A B O U T",False,menucolor_sel,(0,0,0)).convert()            
            text.set_colorkey((0,0,0))
            text.set_alpha(100)
            screen.blit(text, [size[0]//2-text.get_width()//2,size[1]//2-text.get_height()//2])

            text = font2.render("Q U I T",False,menucolor,(0,0,0)).convert()
            edge_left=x>size[0]//2-text.get_width()//2
            edge_right=x<size[0]//2-text.get_width()//2+text.get_width()
            edge_top=y>size[1]//2-text.get_height()//2+100
            edge_bottom=y<size[1]//2-text.get_height()//2+100+text.get_height()
            if edge_left and edge_right and edge_top and edge_bottom:
                text = font2.render("Q U I T",False,menucolor_sel,(0,0,0)).convert()
                if shoot: done=True
            text.set_colorkey((0,0,0))
            text.set_alpha(100)
            screen.blit(text, [size[0]//2-text.get_width()//2,size[1]//2-text.get_height()//2+100])             
            
        else:
            #Если игра не на паузе, т обрабатываем игровую логику
            if pause==False:
                # ВСЯ ИГРОВАЯ ЛОГИКА ДОЛЖНА НАХОДИТЬСЯ ПОД ЭТИМ КОММЕНТАРИЕМ
                #Перебираем все шарики
                for i in balloons:
                    #Сдвигаем каждый шарик вверх
                    i[2]-=i[3]
                    #если шарик улетел вверх, то
                    if i[2]+images['balloon'+str(i[0])].get_height()<0:
                        #Уменьшаем кол-во жизней
                        life-=1
                        if life<1:
                            print('Game Over')
                            done=True
                        #Генерируем новые координаты для шарика
                        i[1]=random.randrange(0,size[0]-images['balloon1'].get_width()-30)
                        i[2]=size[1]
                        #Задаем новую случайную скорость
                        i[3]=random.randint(speed//1.5,speed)
                        #Задаем новый цвет шарика
                        i[0]=random.randint(1,6)
                    #Если игрок выстрелил
                    if shoot:
                        #Если он попал по шарику
    ##                    if x>i[1] and x<i[1]+images['balloon1'].get_width() and y>i[2] and y<i[2]+images['balloon1'].get_height():
                            #Останавливаем предыдущие звуки
                        if x>i[1] and x<i[1]+images['balloon1'].get_width() and y>i[2] and y<i[2]+130:
                            soundBoom1.stop()
                            soundBoom2.stop()
                            #Задаем параметры для дрожжания экрана
                            earthquake=20 #Сдвиг
                            earthquake_time=5#Время дрожжания
                            #Добавляем баллы за подбитый шарик
                            score_cur+=score_add
                            #Если баллов достаточно, то
                            if score_cur>score_next:
                                #Добавлем игроку новую жизнь
                                life+=1
                                #Отодвигаем границу для следующего уровня
                                score_next+=score_step_next
                                #Увеличиваем скорость
                                speed+=1
                                if speed>100:
                                    speed=100
                                #Увеличения уровная
                                level+=1
                                #Выставлям флаг смены уровня
                                level_change=True
                                #Активируем таймер для смены уровня
                                level_change_time=50
                            #Генерируем новые случайные значения для шарика
                            i[3]=random.randint(speed//1.5,speed)#скорость
                            i[0]=random.randint(1,6)#цвет
                            #Генерируем новые координаты для шарика
                            i[1]=random.randrange(size[0]-images['balloon1'].get_width()-30)
                            i[2]=size[1]
                            soundBoom2.play()
                            boom=images['boom2']
                        else:
                            soundBoom1.play()
                            #boom=images['boom1']

                
            # ВСЯ ИГРОВАЯ ЛОГИКА ДОЛЖНА НАХОДИТЬСЯ НАД ЭТИМ КОММЕНТАРИЕМ

            # ВЕСЬ КОД ДЛЯ РИСОВАНИЯ ДОЛЖЕН НАХОДИТЬСЯ ПОД ЭТИМ КОММЕНТАРИЕМ
            # Очистить экран и установить фон
            if shoot:
                if boom==images['boom1']:
                    images['fon'].blit(boom,[x-boom.get_width() // 2,y-boom.get_height() // 2])
                screen.blit(boom,[x-boom.get_width() // 2,y-boom.get_height() // 2])
                
                
            screen.blit(images['fon'],[0,0])
            screen.blit(images['fon'], [0+earthquake,0+earthquake])
            screen.blit(images['paper1'],[0+earthquake,0+earthquake])
            
            if shoot:
                if boom!=images['boom1']:
                    screen.blit(boom,[x-boom.get_width() // 2,y-boom.get_height() // 2])

            for i in balloons:
                screen.blit(images['balloon'+str(i[0])],[i[1]+earthquake,i[2]+earthquake])
    ##            text = font.render(str(i[3]),True,(0,0,255))
    ##            screen.blit(text, [i[1],i[2]])


            #Отображаем пользовательский курсор
            screen.blit(images['cursor'],[x,y])
            
            #Выводим информацию о заработанных очках, текущем уровне и т.п.        
            text = font.render("Score "+str(score_cur),True,(0,0,255))
            if earthquake>0:
                screen.blit(text, [10+earthquake,10+earthquake])
            else:
                screen.blit(text, [10,10])
            text = font.render("Life "+str(life),True,(0,0,255))
            if earthquake>0:
                screen.blit(text, [10+earthquake,30+earthquake])
            else:
                screen.blit(text, [10,30])
            text = font.render("Level "+str(level),True,(0,0,255))
            if earthquake>0:
                screen.blit(text, [10+earthquake,50+earthquake])
            else:
                screen.blit(text, [10,50])        


            

            if pause==True:
                text = font2.render("P A U S E",False,(0,0,255),(0,0,0)).convert()
                text.set_colorkey((0,0,0))
                text.set_alpha(100)
                screen.blit(text, [size[0]//2-text.get_width()//2,size[1]//2-text.get_height()//2])
    ##            print(text.get_alpha())

            if level_change_time>0:
                text = font2.render("N E W  L E V E L   "+str(level),False,(255,0,0),(0,0,0)).convert()
                text.set_colorkey((0,0,0))
                text.set_alpha(level_change_time*30)
                screen.blit(text, [size[0]//2-text.get_width()//2,size[1]//2-text.get_height()//2])
                level_change_time-=1
            else:
                level_change=not(level_change)
        # Обновить экран, выведя то, что мы нарисовали
        pygame.display.flip()
        # ВЕСЬ КОД ДЛЯ РИСОВАНИЯ ДОЛЖЕН НАХОДИТЬСЯ НАД ЭТИМ КОММЕНТАРИЕМ

        # Ограничить до 20 кадров в секунду
        clock.tick(20)

    #Выход/завершение работы pygame
    pygame.quit()


#Вызов основной функции
if __name__ == '__main__': main()
