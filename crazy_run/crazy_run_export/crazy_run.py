#-*-coding:utf-8-*-
#!/usr/bin/python
# Выполнил: Ляш Олег Иванович
'''
Пример игры с перемещением объектов и движением персонажа по платформе
'''
# Released under the GNU General Public License

VERSION = "0.3"

try:
    import random
    import math
    import os
    import pygame
    from pygame.locals import *
except ImportError, err:
    print "Не могу загрузить модуль %s" % (err)
    sys.exit(2)

game_info={'size':[800,600],
           'screen':None,
           'path':'data/',
           'background':(155,209,244),
           'font':None,
           'font1':None,
           'font1':None}
           
game_state={'done':False,
            'debug':True,
            'first_run':True}
            
game_sound={'music':None,
            'sound1':None,
            'sound2':None,}
game_img={'fon':None,
          'hero1':None}                    
game_keys={'left':False,
           'up':False,
           'right':False,
           'down':False,
           'jump':False}

Game_objects={}        

player={'x':0,
        'y':0,
        'xs':0,
        'ys':0,
        'life':10,
        'level':1
        }

def load_png(name):
        ''' Загружает изображение
        возвращает объект-изображение '''
        fullname = os.path.join('data/img/', name)
        try:
                image = pygame.image.load(fullname)
                if image.get_alpha is None:
                        image = image.convert()
                else:
                        image = image.convert_alpha()
        except pygame.error, message:
                print 'Не могу загрузить изображение:', fullname
                raise SystemExit, message
        return image

class Game_obj:
    def __init__(self, img, x, y):
        self.image = load_png(img)
        self.start_x = x
        self.start_y = -400
        self.x = x
        self.y = y
        self.sx=-random.gammavariate(3,7)
        #if random.randrange(-10,10)<0:self.sx=self.sx*-1
        self.sy=5 #random.gammavariate(2,4)
        self.coord = (x, y)
    def move(self):
        screen = pygame.display.get_surface()
        self.x+=self.sx
        if self.sx<0:
            if self.x+self.image.get_width()<0:
                self.x=screen.get_width()
        else:
            if self.x>screen.get_width():
                self.x=0-self.image.get_width()
    def draw(self,first_run=False):
        screen = pygame.display.get_surface()        
        if first_run==False or self.start_y>=self.y :
            screen.blit(self.image,[self.x,self.y])
        else:
            self.start_y+=self.sy
            screen.blit(self.image,[self.x,self.start_y])            

         

def init(Fullscreen=False):
    global game_info,game_img, game_sound
    pygame.init()
    if Fullscreen:
        game_info['size']=[pygame.display.list_modes()[0][0],pygame.display.list_modes()[0][1]]
        game_info['screen']=pygame.display.set_mode(game_info['size'], pygame.FULLSCREEN)
    else:
        #game_info['size']=[pygame.display.list_modes()[1][0],pygame.display.list_modes()[1][1]]
        game_info['size']=[1024,768]
        game_info['screen']=pygame.display.set_mode(game_info['size'])
    pygame.display.set_caption("Crazy run")
    pygame.mixer.music.load(game_info['path']+'snd/music1.mp3')    
    pygame.mixer.music.play()
    

    game_img['hero2']=pygame.image.load(game_info['path']+"img/hero2.png").convert_alpha()    
    game_img['fon']=pygame.image.load(game_info['path']+"img/fon.jpg").convert()    
    game_img['fon']=pygame.transform.scale(game_img['fon'],(game_info['size'][0],game_info['size'][0]))
    game_img['ground']=pygame.image.load(game_info['path']+"img/ground.png").convert_alpha()    
        
    pygame.font.init()
    game_info['font'] = pygame.font.Font('data/fonts/Metro.otf', 12)  
    
    Game_objects['cloud']=Game_obj('cloud1.png',800,20)
    Game_objects['sun']=Game_obj('sun.png',10,10)
    Game_objects['mount1']=Game_obj('mount1.png',10,300)
    Game_objects['mount2']=Game_obj('mount1.png',200,400)    
    Game_objects['mount3']=Game_obj('mount1.png',600,500)      
    Game_objects['hero']=Game_obj('hero2.png',100,game_info['size'][1]-game_img['ground'].get_height()-170)      
    

def draw_scene():
    screen=pygame.display.get_surface()
    screen.fill(game_info['background'])
    w=screen.get_width()
    h=screen.get_height()
    z=100
    screen.blit(game_img['fon'],[0,0])
    
    Game_objects['mount1'].draw(game_state['first_run'])
    Game_objects['mount2'].draw(game_state['first_run'])
    Game_objects['mount3'].draw(game_state['first_run'])
    Game_objects['sun'].draw(game_state['first_run'])
    Game_objects['cloud'].move()
    Game_objects['cloud'].draw(game_state['first_run'])  
    if Game_objects['mount3'].y<=Game_objects['mount3'].start_y:
        game_state['first_run']=False    

    screen.blit(game_img['ground'],[-5,game_info['size'][1]-game_img['ground'].get_height()+20])
    Game_objects['hero'].draw()
    if game_state['debug']:
        text = game_info['font'].render(u"Влево : "+str(game_keys['left']),False,(255,0,0))
        screen.blit(text, [20,20])    
        text = game_info['font'].render(u"Up : "+str(game_keys['up']),False,(255,0,0))
        screen.blit(text, [20,50])    
        text = game_info['font'].render(u"Right : "+str(game_keys['right']),False,(255,0,0))
        screen.blit(text, [20,80])    
        text = game_info['font'].render(u"Down : "+str(game_keys['down']),False,(255,0,0))
        screen.blit(text, [20,110])            
        text = game_info['font'].render(u"Jump : "+str(game_keys['jump']),False,(255,0,0))
        screen.blit(text, [20,140])                    
    
    
def events():
    for event in pygame.event.get():
        #Если пользователь закрывает программу (крестик в окне, ALT+F4 и т.п.)
        if event.type == pygame.QUIT:
            game_state['done']=True
        # если нажал на клавишу ESC
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                game_state['done']=True     
            elif event.key == pygame.K_F1:
                game_state['debug']=not(game_state['debug'])
            elif event.key == pygame.K_LEFT:
                game_keys['left'] = True
            elif event.key == pygame.K_UP:
                game_keys['up'] = True
            elif event.key == pygame.K_RIGHT:
                game_keys['right'] = True
            elif event.key == pygame.K_DOWN:
                game_keys['down'] = True
            elif event.key == pygame.K_SPACE:
                game_keys['jump'] = True                
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                game_keys['left'] = False
            elif event.key == pygame.K_UP:
                game_keys['up'] = False
            elif event.key == pygame.K_RIGHT:
                game_keys['right'] = False
            elif event.key == pygame.K_DOWN:
                game_keys['down'] = False
            elif event.key == pygame.K_SPACE:
                game_keys['jump'] = False

def move_game_objects():
    if game_keys['right']:
        Game_objects['mount1'].move()
        Game_objects['mount2'].move()
        Game_objects['mount3'].move()
        
        
        
def main():
    #Инициализация игры (если True, то в полноэкранном режиме)    
    init(True)
    while game_state['done']==False:
        #Обработка событий 
        events()
        move_game_objects()
        #Отрисовываем интерфейс
        draw_scene()         

                    
            #Вывод персонажа на экран
#            game_info['screen'].blit(game_img['hero1'],[100,300])

        #        game_info['screen'].blit(game_img['sun'],[20,20])
        #        game_info['screen'].blit(game_img['cloud'],[200,20])

        #
        pygame.display.flip()
    
main()
