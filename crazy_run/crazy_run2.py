#-*-coding:utf-8-*-
#!/usr/bin/python
# Выполнил: Ляш Олег Иванович
'''
Пример игры с перемещением объектов и движением персонажа по платформе
'''
# Released under the GNU General Public License

VERSION = "0.3"

try:
    import random
    import os
    import pygame
    from pygame.locals import *
except ImportError, err:
    print "Не могу загрузить модуль %s" % (err)
    sys.exit(2)
#Словарь, хранящий информацию об игре (размер экрана/окна по умолчанию, объект-ЭКРАН, путь к ресурсам игр, объект-шрифт)
game_info={'size':[800,600],
           'screen':None,
           'path':'data/',
           'font':None}
#Словарь, хранящий состояние игры (игра запущена, первый запуск)           
game_state={'done':False,
            'first_run':True}

#Некоторые игровые изображения (фон, земля). Эти объекты не двигаются
game_img={'fon':None,
          'ground':None}                    
#Словарь, хранящий состояние управляющих клавиш
game_keys={'left':False,
           'up':False,
           'right':True,
           'down':False,
           'jump':False}
#Пустой словарь в который позднее будут добавлены игровые объекты
Game_objects={}        
#Функция, выполняющая загрузку изображений
def load_png(name):
        ''' Загружает изображение
        возвращает объект-изображение '''
        fullname = os.path.join('data/img/', name)
        try:
            image = pygame.image.load(fullname)
            if image.get_alpha is None:
                    image = image.convert()
            else:
                    image = image.convert_alpha()
        except pygame.error, message:
            print 'Не могу загрузить изображение:', fullname
            raise SystemExit, message
        return image
#### Далее описываются классы игровых объектов
class TGame_obj:
    '''Игровой объект с общими для всех игровых объектов свойствами и методами
    '''
    def __init__(self, img, x, y):
        '''Инициализация игрового объекта
        '''
        self.image = load_png(img) #Загрузка изображения
        self.start_y = -400 # Начальная координата объекта по оси OY (отрицательное число взято для того, чтобы объект в начале игры падал сверху)
        self.x = x # Координата объекта по оси OX
        self.y = y # Координата объекта по оси OY
        self.xs=-random.gammavariate(3,7) # Скорость движения объекта по оис OX
        self.ys=5 # Координата объекта по оси OY
    def move(self):
        '''Движение объекта
        '''
        pass # т.к. это базовый класс, то никаких действий по описанию движения оъекта не делаем
    def draw(self,first_run=False):
        '''Отрисовка объекта на экране
        '''
        screen = pygame.display.get_surface() #Получаем объект-экран
        screen.blit(self.image,[self.x,self.start_y])# Выводим наш объект на экран

class TMount(TGame_obj):
    '''Гора
    '''
    def move(self):
        '''Движение объекта
        '''
        screen = pygame.display.get_surface() #Получаем объект-экран
        self.x+=self.xs #Сдвигаем объект на величину смещения
        if self.xs<0: #Если смещение отрицательное, то 
            #проверяем не вылез ли объект за левую границу
            if self.x+self.image.get_width()<0:
                self.x=screen.get_width()
        else:#в противном случае, если смещение положительное
            #то проверяем не вылез ли объект за правую границу
            if self.x>screen.get_width():
                self.x=0-self.image.get_width()
    def draw(self,first_run=False):
        '''Отрисовка объекта на экране
        '''
        screen = pygame.display.get_surface()#Получаем объект-экран
        #Если это не первый запуск игры, то
        if first_run==False or self.start_y>=self.y :
            #рисуем объект в координатах x,y
            screen.blit(self.image,[self.x,self.y])
        else:# в противном случае (т.е. если это первый запуск)
            # сдвигаем объект вниз относительно стартовой позиции
            self.start_y+=self.ys
            #рисуем объект в координатах x,start_y
            screen.blit(self.image,[self.x,self.start_y])                

class TCloud(TGame_obj):
    '''Облако (все методы здесь идентичны классу ГОРА)
    '''
    def move(self):
        screen = pygame.display.get_surface()
        self.x+=self.xs
        if self.xs<0:
            if self.x+self.image.get_width()<0:
                self.x=screen.get_width()
        else:
            if self.x>screen.get_width():
                self.x=0-self.image.get_width()
    def draw(self,first_run=False):
        screen = pygame.display.get_surface()        
        if first_run==False or self.start_y>=self.y :
            screen.blit(self.image,[self.x,self.y])
        else:
            self.start_y+=self.ys
            screen.blit(self.image,[self.x,self.start_y]) 
            
class TPlayer():
    '''Игрок
    '''
    def __init__(self,img,x,y):
        '''Инициализация
        '''
        self.image = load_png(img)
        self.start_x = x
        self.start_y = -400
        self.x = x
        self.y = y
        self.ys=3
        #Переменные, описывающие этапы прыжка        
        self.jump_height=50 #Максимальная высота прыжка
        self.ys_limit=self.jump_height #Переменная счетчик, используемя для определения этапа прыжка
        self.jump_up=False #Прыжок вверх
        self.jump_down=False #Падение после прыжка
        
    def move(self):
        screen = pygame.display.get_surface()
        #Реализция прыжка.
        '''
        Когда пользователь нажимает на прыжок считаем, что прыжок начался (jump_up=True). 
        Персонаж прыгает вверх (т.е. движется вверх) до тех пор пока не истечет время (лимит прыжка), т.е. пока ys_limit
        не станет меньше или равно нулю.
        Как только ys_limit станет равным нулю мы переключаем этап прыжка, т.е. заставляем персонаж падать вниз (jump_down=True)
        И этот этап длится пока не истечет лимит времени
        '''        
        if self.jump_up:
            self.y-=self.ys
            self.ys_limit-=1
            if self.ys_limit<=0:
                self.jump_up=False
                self.jump_down=True
                self.ys_limit=self.jump_height
        if self.jump_down:
            self.y+=self.ys
            self.ys_limit-=1
            if self.ys_limit<=0:
                self.jump_up=False
                self.jump_down=False
                self.ys_limit=self.jump_height
    def draw(self):
        screen = pygame.display.get_surface()        
        screen.blit(self.image,[self.x,self.y])
    

def init(Fullscreen=False):
    '''Инициализация игры
    '''
    global game_info,game_img, game_sound
    pygame.init() #Инициализация pygame
    if Fullscreen:#Если выбран запуск игры в полноэкранном режиме, то
        #Получаем максимальное разрешение экрана
        game_info['size']=[pygame.display.list_modes()[0][0],pygame.display.list_modes()[0][1]]
        #Запускаем игру в полноэкранном режиме
        game_info['screen']=pygame.display.set_mode(game_info['size'], pygame.FULLSCREEN)
    else:# в противном случае
        #устаналиваем нужное нам разрешение
        game_info['size']=[1024,768]
        #запускаем игру в оконном режиме
        game_info['screen']=pygame.display.set_mode(game_info['size'])
    #Устанавливаем заголовок окна
    pygame.display.set_caption("Crazy run")
    #Загружаем музыку
    pygame.mixer.music.load(game_info['path']+'snd/music1.mp3')    
    #Запускаем музыку на воспроизведение
    pygame.mixer.music.play()
    
    #Загружаем некоторые изображения
    #Фоновая картинка
    game_img['fon']=pygame.image.load(game_info['path']+"img/fon.jpg").convert()    
    #Масштабируем фоновую картинку под размеры окна
    game_img['fon']=pygame.transform.scale(game_img['fon'],(game_info['size'][0],game_info['size'][0]))
    #Подгружаем землю
    game_img['ground']=pygame.image.load(game_info['path']+"img/ground.png").convert_alpha()    
    #Инициализируем модуль шрифтов
    pygame.font.init()
    #Активируем выбранный шрифт 
    game_info['font'] = pygame.font.Font('data/fonts/Metro.otf', 12)  
    
    #Создаем игровые объекты
    Game_objects['cloud']=TCloud('cloud1.png',800,20) #Облако
    Game_objects['sun']=TGame_obj('sun.png',10,10) #Солнце
    mount_height=627#Это высота картинки с горой
    Game_objects['mount1']=TMount('mount1.png',10,game_info['size'][1]-mount_height)  #Гора1
    Game_objects['mount2']=TMount('mount1.png',200,game_info['size'][1]-mount_height) #Гора2
    Game_objects['mount3']=TMount('mount1.png',600,game_info['size'][1]-mount_height) #Гора3
    #Сдвигаем две горы немного вниз
    Game_objects['mount2'].y=game_info['size'][1]-mount_height-mount_height//6
    Game_objects['mount3'].y=game_info['size'][1]-mount_height-mount_height//5
    #Создаем игрового персонажа
    Game_objects['hero']=TPlayer('hero2.png',100,game_info['size'][1]-game_img['ground'].get_height()-170)      
    

def draw_scene():
    '''Построение сцены из игровых объектов
    '''
    screen=pygame.display.get_surface() #Получаем объект-экран
    #Выводим фон
    screen.blit(game_img['fon'],[0,0])
    #Выводим остальные игровые объекты
    Game_objects['mount1'].draw(game_state['first_run'])
    Game_objects['mount2'].draw(game_state['first_run'])
    Game_objects['mount3'].draw(game_state['first_run'])
    Game_objects['sun'].draw(game_state['first_run'])
    Game_objects['cloud'].draw(game_state['first_run'])  
    #Если последняя гора при первом запуске упала вниз, то выключаем флаг первого запуска
    if Game_objects['mount3'].y<=Game_objects['mount3'].start_y:
        game_state['first_run']=False
    #Выводим землю
    screen.blit(game_img['ground'],[-5,game_info['size'][1]-game_img['ground'].get_height()+20])
    #Выводим персонажа
    Game_objects['hero'].draw()
    
def events():
    '''Обработка событий
    '''
    for event in pygame.event.get():
        #Если пользователь закрывает программу (крестик в окне, ALT+F4 и т.п.)
        if event.type == pygame.QUIT:
            game_state['done']=True
        # если нажал на клавишу ESC
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                game_state['done']=True     
            elif event.key == pygame.K_RIGHT:
                game_keys['right'] = True
            elif event.key == pygame.K_SPACE:
                game_keys['jump'] = True                
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                game_keys['right'] = False

def move_game_objects():
    '''Свдигаем игровые объекты
    '''
    #Если это уже не первый запуск, то 
    if game_state['first_run']==False:
        #Вызываем сдвиг объектов
        Game_objects['cloud'].move()
        if game_keys['right']:
            Game_objects['mount1'].move()
            Game_objects['mount2'].move()
            Game_objects['mount3'].move()
        if game_keys['jump']==True and Game_objects['hero'].jump_up==False and Game_objects['hero'].jump_down==False:
            Game_objects['hero'].jump_up=True
            game_keys['jump']=False
        Game_objects['hero'].move()
                
def main():
    #Инициализация игры (если True, то в полноэкранном режиме)    
    init(True)
    while game_state['done']==False:
        #Обработка событий 
        events()
        move_game_objects()
        #Рисуем на экране всю, подготовленную ранее сцену со всеми игровыми объектами
        draw_scene()         
        pygame.display.flip()
    
main()
