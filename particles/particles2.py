#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Пример реализации системы частиц
'''
####
# Импортировать библиотеку под названием 'pygame'
import pygame, random
#Графитация
grav=9.8
#Класс, описывающий данные частицы
class particle:
    def __init__(this):
        this.x=0
        this.y=0
        this.xs=0.1
        this.ys=0.1
        this.r=255
        this.g=255
        this.b=255      
    def set_particle(this,x=100,y=100):
        this.r=random.randrange(1,255)
        this.g=random.randrange(1,255)
        this.b=random.randrange(1,255)
        this.x=x
        this.y=y
        this.xs=random.randrange(1,5)
        this.xs=random.gammavariate(1,5)
        if random.randrange(-10,10)<0:this.xs=this.xs*-1
        this.ys=random.gammavariate(1,5)
        if random.randrange(-10,10)>0:this.ys=this.ys*-1        
    def move(this,x,y):
        this.x+=this.xs
        this.y+=this.ys
        this.y+=grav
        this.r-=10
        this.g-=10
        this.b-=10           
        if this.r<1 or this.g<1 or this.b<1:
            this.set_particle(x,y)        
    def draw(this,screen):
        pygame.draw.circle(screen,(this.r,this.g,this.b),(int(this.x),int(this.y)),3)
    
        
def main():
    # Инициализировать игровой движок
    pygame.init()
    # Определение некоторых цветов
    black = ( 0, 0, 0)
    white = ( 255, 255, 255)
    # Задать ширину и высоту экрана
    size=[800,600]
    #Собственно создание экрана с заданными размерами
    screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("Particles")
    #Количество частиц
    particles_count=1000
    #Массив/список для хранения частиц
    particles=[]
    #Цикл для заполнения массива частицами
    for i in range(particles_count):
        #Добавляем новый экземпляр класса в массив
        particles.append(particle())
        #Задаем стартовые параметры для частицы
        particles[i].set_particle(size[0]//2,size[1]//2)
        print(particles[i].r)
        

    
    clock=pygame.time.Clock()   
    done=False   
    # -------- Основной цикл программы -----------    
    while done==False:
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    done=True
                if event.key == pygame.K_SPACE:
                    pause=not(pause)#ставим игру на паузу
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    shoot=True
                    
        pos = pygame.mouse.get_pos()
        x=pos[0]
        y=pos[1]
        
        screen.fill(black)
        
        for i in particles:            
            i.move(x,y)
            i.draw(screen)
        pygame.display.flip()
        # Ограничить до 20 кадров в секунду
        clock.tick(20)
    #Выход/завершение работы pygame
    pygame.quit()


#Вызов основной функции
if __name__ == '__main__': main()

