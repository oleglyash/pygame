#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Пример реализации системы частиц
'''
####
# Импортировать библиотеку под названием 'pygame'
import pygame, random

#Класс, описывающий данные частицы
class particle:
    def __init__(this):
        this.x=0
        this.y=0
        this.xs=0.1
        this.ys=0.1
        this.r=255
        this.g=255
        this.b=255
        
def main():
    # Инициализировать игровой движок
    pygame.init()
    # Определение некоторых цветов
    black = ( 0, 0, 0)
    white = ( 255, 255, 255)
    #Графитация
    grav=9.8
    # Задать ширину и высоту экрана
    size=[800,600]
    #Собственно создание экрана с заданными размерами
    screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("Particles")
    #Количество частиц
    particles_count=1000
    #Массив/список для хранения частиц
    particles=[]
    #Цикл для заполнения массива частицами
    for i in range(particles_count):
        #Добавляем новый экземпляр класса в массив
        particles.append(particle())
        #Задаем стартовые параметры для частицы
        particles[i].r=random.randrange(1,255)
        particles[i].g=random.randrange(1,255)
        particles[i].b=random.randrange(1,255)
        particles[i].x=size[0]//2
        particles[i].y=size[1]//2
        particles[i].xs=random.randrange(1,5)
        particles[i].xs=random.gammavariate(1,5)
        if random.randrange(-10,10)<0:particles[i].xs=particles[i].xs*-1
        particles[i].ys=random.gammavariate(1,5)
        if random.randrange(-10,10)>0:particles[i].ys=particles[i].ys*-1    
    
    clock=pygame.time.Clock()   
    done=False   
    # -------- Основной цикл программы -----------    
    while done==False:
        pos = pygame.mouse.get_pos()
        x=pos[0]
        y=pos[1]
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    done=True
                if event.key == pygame.K_SPACE:
                    pause=not(pause)#ставим игру на паузу
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    shoot=True
        screen.fill(black)
        
        for i in particles:
            i.x+=i.xs
            i.y+=i.ys
            i.y+=grav
            i.r-=10
            i.g-=10
            i.b-=10           
            if i.r<1 or i.g<1 or i.b<1:
#                i.grav=random.randrange(1,10)
#                i.grav=random.gammavariate(1,2)
                i.r=random.randrange(1,255)
                i.g=random.randrange(1,255)
                i.b=random.randrange(1,255)
                i.x=x
                i.y=y
                i.xs=random.randrange(1,5)
                i.xs=random.gammavariate(1,5)
                if random.randrange(-10,10)<0:i.xs=i.xs*-1
                i.ys=random.gammavariate(1,5)
                if random.randrange(-10,10)>0:i.ys=i.ys*-1              
##                if i.xs==0: i.xs=1

##                if i.ys==0: i.ys=1
##            pygame.draw.rect(screen,(r,g,b),(x+i.x,y+i.y,x+i.x+1,y+i.y+1))
            pygame.draw.circle(screen,(i.r,i.g,i.b),(int(i.x),int(i.y)),3)
        
        pygame.display.flip()
        # ВЕСЬ КОД ДЛЯ РИСОВАНИЯ ДОЛЖЕН НАХОДИТЬСЯ НАД ЭТИМ КОММЕНТАРИЕМ

        # Ограничить до 20 кадров в секунду
        clock.tick(20)

    #Выход/завершение работы pygame
    pygame.quit()


#Вызов основной функции
if __name__ == '__main__': main()
