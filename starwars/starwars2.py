#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Компьютерная игра "StarWars".
Игорку необходимо подбивать всех врагов, которые появляются сверху экрана.
'''
####

# Импортировать библиотеку под названием 'pygame'
import pygame
import random

    
def main():
    ###Инициализация и загрузка данных
    path="data/img/"
    max_speed=30
    speed=1
    fullscreen=False
    # Инициализировать игровой движок
    pygame.init()
    # Задать ширину и высоту экрана
    #pygame.display.list_modes()[0][0] - это максимальное разрещение экрана по оси OX
    #pygame.display.list_modes()[0][1] - это максимальное разрещение экрана по оси OY
    if fullscreen==True:
        size=[pygame.display.list_modes()[0][0],
              pygame.display.list_modes()[0][1]]
        screen=pygame.display.set_mode(size, pygame.FULLSCREEN)
    else:
        size=[pygame.display.list_modes()[len(pygame.display.list_modes())//2][0],
              pygame.display.list_modes()[len(pygame.display.list_modes())//2][1]]
        screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("StarWars")
    #Установка иконки для окна
    icon=pygame.image.load(path+"rocket.png").convert_alpha()
    icon=pygame.transform.scale(icon,(32,32))
    pygame.display.set_icon(icon)
    #Отключение отображения указателя мыши
    pygame.mouse.set_visible(0)
    #Инициализация шрифтов
    pygame.font.init()
    # Используется для контроля частоты обновления экрана
    clock=pygame.time.Clock()    
    
    #Загрузка изображений
    images={'fon':pygame.image.load(path+"fon1.jpg").convert(),
            'rocket':pygame.image.load(path+"rocket.png").convert_alpha(),
            'asteroid':pygame.image.load(path+"asteroid.png").convert_alpha(),
            'boom':pygame.image.load(path+"boom.png").convert()
            }
    images['boom'].set_colorkey((0,0,0))
    images['boom'].set_alpha(255)
    #Масштабируем фоновую картинку под текущий размер окна
    images['fon']=pygame.transform.scale(images['fon'],(screen.get_width(),screen.get_height()))
    #Загружаем фоновую картинку в буфер. Буфер используем для прокрутки фона
    buf=pygame.image.load("data/img/fon1.jpg").convert()

    #Создаем игровой объект и задаем его параметры
    rocket={'x':0,'y':300}

    #Астероид
    asteroid={'x':100,
              'y':random.randint(1,screen.get_height()-images['asteroid'].get_height()),
              'speed':2,
              'angle':0}
    
    boom=False
    boom_time=0
    boom_max_time=50
    #Переменные для фиксации нажатых клавиш
    #по умолчанию ничего не нажато
    keyup=False
    keydown=False
    keyspace=False


    done=False
    while done==False:
       # Обработка событий
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                done=True
            #Если пользователь нажал на кнопку
            if event.type == pygame.KEYDOWN:
                #Если нажата кнопка выхода (Escape)
                if event.key == pygame.K_ESCAPE:
                    done=True # то установить флаг для выхода
                #Если нажали кнопку вверх
                elif event.key == pygame.K_UP:
                    keyup=True # то выставляем флаг нажатия кнопки вверх
                elif event.key == pygame.K_DOWN:
                    keydown=True
                elif event.key == pygame.K_SPACE:
                    keyspace=True
            #Если пользователь Отпустил кнопку
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    keyup=False
                elif event.key == pygame.K_DOWN:
                    keydown=False
                elif event.key == pygame.K_SPACE:
                    keyspace=False
        #Обработка логики игры
        if keyup:
            rocket['y']-=10
            if rocket['y']<0:
                rocket['y']=0
        elif keydown:
            rocket['y']+=10
            if rocket['y']+images['rocket'].get_height()>screen.get_height():
                rocket['y']=screen.get_height()-images['rocket'].get_height()
        if keyspace:
            speed+=1
            if speed>max_speed:speed=max_speed
        else:
            speed-=1
            if speed<1:speed=1

        #Сдвигаем астероид
        asteroid['x']-=speed
        #Если астероид залетел за левую границу экрана, то
        if asteroid['x']+images['asteroid'].get_width()<0:
            #перемещаем его за правую границу экрана
            asteroid['x']=screen.get_width()
            #генерируем случайную координату по оси OY
            asteroid['y']=random.randint(1,screen.get_height()-images['asteroid'].get_height())

        #Анализируем столкновение кораблика и астероида
        edge_left=(rocket['x']>asteroid['x'] and
                   rocket['x']<asteroid['x']+images['asteroid'].get_width())
        edge_right=(rocket['x']+images['rocket'].get_width()>asteroid['x'] and
                    rocket['x']+images['rocket'].get_width()<asteroid['x']+images['asteroid'].get_width())

        edge_top=(rocket['y']>asteroid['y'] and
                   rocket['y']<asteroid['y']+images['asteroid'].get_height())
        edge_bottom=(rocket['y']+images['rocket'].get_height()>asteroid['y'] and
                    rocket['y']+images['rocket'].get_height()<asteroid['y']+images['asteroid'].get_height())
        
        if (edge_left or edge_right) and (edge_top or edge_bottom):
               boom=True


        #Вывод на экран
            
        #Прокрутка справа налево
        buf.blit(images['fon'],[0,0],[speed,0,images['fon'].get_width(),images['fon'].get_height()])
        buf.blit(images['fon'],[images['fon'].get_width()-speed,0],[0,0,speed,images['fon'].get_height()])

        #Рисуем фон
        images['fon'].blit(buf,[0,0])
        screen.blit(images['fon'],[0,0])

        #Выводим картинку с ракетой
        if boom==False:
            screen.blit(images['rocket'],[rocket['x'],rocket['y']])
            #Выводим картинку с астероидом
            screen.blit(images['asteroid'],[asteroid['x'],asteroid['y']])            
        else:
##            screen.blit(images['boom'],[rocket['x'],rocket['y']])
            width=images['rocket'].get_width()+boom_time
            height=images['rocket'].get_height()+boom_time
            img=pygame.transform.scale(images['boom'],(width,height))
            img.set_alpha(100-boom_time*2)
            screen.blit(img,[rocket['x'],rocket['y']])            
            boom_time+=1
        if boom_time>boom_max_time:
            boom_time=0
            boom=False
            #перемещаем его за правую границу экрана
            asteroid['x']=screen.get_width()
            #генерируем случайную координату по оси OY
            asteroid['y']=random.randint(1,screen.get_height()-images['asteroid'].get_height())            
            

        

        pygame.display.flip()
        clock.tick(20)

    pygame.quit()

main()
