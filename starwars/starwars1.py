#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Компьютерная игра "Battle ship".
Игорку необходимо подбивать всех врагов, которые появляются сверху экрана.
'''
####

# Импортировать библиотеку под названием 'pygame'
import pygame
import random
import os
path="data/img/"
max_speed=1   
###Инициализация и загрузка данных
def init(fullscreen=False):
    global path,clock
    # Инициализировать игровой движок
    pygame.init()
    # Задать ширину и высоту экрана
    #pygame.display.list_modes()[0][0] - это максимальное разрещение экрана по оси OX
    #pygame.display.list_modes()[0][1] - это максимальное разрещение экрана по оси OY
    if fullscreen==True:
        size=[pygame.display.list_modes()[0][0],pygame.display.list_modes()[0][1]]
        screen=pygame.display.set_mode(size, pygame.FULLSCREEN)
    else:
        size=[pygame.display.list_modes()[len(pygame.display.list_modes())//2][0],
              pygame.display.list_modes()[len(pygame.display.list_modes())//2][1]]
        screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("Balloons")
    #Установка иконки для окна
    icon=pygame.image.load(path+"rocket.png").convert_alpha()
    icon=pygame.transform.scale(icon,(32,32))
    pygame.display.set_icon(icon)
    #Отключение отображения указателя мыши
    pygame.mouse.set_visible(0)
    #Инициализация шрифтов
    pygame.font.init()
    # Используется для контроля частоты обновления экрана
    clock=pygame.time.Clock()    
    return screen
    

def main():
    clock=pygame.time.Clock()  
    
    screen=init()
    done=False

    bg=pygame.image.load("data/img/fon1.jpg").convert()
    bg=pygame.transform.scale(bg,(screen.get_width(),screen.get_height()))

    buf=pygame.image.load("data/img/fon1.jpg").convert()
    
    rocket=pygame.image.load("data/img/rocket.png").convert_alpha()
    
    w=bg.get_width()
    h=bg.get_height()

    asteroid=pygame.image.load("data/img/asteroid.png").convert_alpha()
    ax=100
    ay=random.randint(1,h-asteroid.get_height())
    aspeed=2
    
    x=0
    y=300
    keyup=False
    keydown=False
    keyspace=False
    s=1
    max_s=30
    while done==False:
       # ОБРАБОТКА ВСЕХ СОБЫТИЙ ДОЛЖНА БЫТЬ ПОД ЭТИМ КОММЕНТАРИЕМ
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                done=True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    done=True
                elif event.key == pygame.K_UP:
                    keyup=True
                elif event.key == pygame.K_DOWN:
                    keydown=True
                elif event.key == pygame.K_SPACE:
                    keyspace=True
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    keyup=False
                elif event.key == pygame.K_DOWN:
                    keydown=False
                elif event.key == pygame.K_SPACE:
                    keyspace=False                    
        if keyup:
            y-=10
            if y<0: y=0
        elif keydown:
            y+=10
            if y+rocket.get_height()>screen.get_height():y=screen.get_height()-rocket.get_height()
        if keyspace:
            s+=1
            if s>max_s:s=max_s
        else:
            s-=1
            if s<1:s=1
        #Прокрутка слева на право
##        buf.blit(bg,[0,0],[bg.get_width()-s,0,bg.get_width(),bg.get_height()])
##        buf.blit(bg,[s,0],[0,0,bg.get_width()-s,bg.get_height()])
        #Прокрутка справа налево
        buf.blit(bg,[0,0],[s,0,bg.get_width(),bg.get_height()])
        buf.blit(bg,[bg.get_width()-s,0],[0,0,s,bg.get_height()])
        
        bg.blit(buf,[0,0])

        screen.blit(bg,[0,0])

        screen.blit(asteroid,[ax,ay])
        ax-=s
        if ax+asteroid.get_width()<0:
            ax=w+asteroid.get_width()
            ay=random.randint(1,h-asteroid.get_height())

        
        screen.blit(rocket,[x,y])
        

        pygame.display.flip()
        clock.tick(20)

    pygame.quit()

main()
