#-*-coding:utf-8-*-
# Выполнил: Ляш Олег Иванович
'''
Компьютерная игра "StarBattle".
Игорку необходимо подбивать всех врагов, которые появляются сверху экрана.
'''
####

# Импортировать библиотеку под названием 'pygame'
import pygame, random
   
def main():
    ###Инициализация и загрузка данных
    path="data/"
    # Инициализировать игровой движок
    pygame.init()
    # Задать ширину и высоту экрана
    size=[800,600]
    screen=pygame.display.set_mode(size)
    #Установка текста в заголовке окна
    pygame.display.set_caption("StarBattle")
    #Установка иконки для окна
    icon=pygame.image.load(path+"img/rocket.png").convert_alpha()
    icon=pygame.transform.scale(icon,(32,32))
    pygame.display.set_icon(icon)
    #Отключение отображения указателя мыши
    pygame.mouse.set_visible(0)
    #Инициализация шрифтов
    pygame.font.init()
    # Используется для контроля частоты обновления экрана
    clock=pygame.time.Clock()    
    
    #Загрузка изображений
    images={'fon':pygame.image.load(path+"img/fon1.jpg").convert(),
            'rocket':pygame.image.load(path+"img/rocket.png").convert_alpha()
            }
    #Масштабируем фоновую картинку под текущий размер окна
    images['fon']=pygame.transform.scale(images['fon'],(screen.get_width(),screen.get_height()))
    #Загружаем фоновую картинку в буфер. Буфер используем для прокрутки фона
    buf=pygame.image.load("data/img/fon1.jpg").convert()

    #Загружаем звуки
    pygame.mixer.music.load(path+'sound/DestroyHead_-_Space_Things.mp3')
    pygame.mixer.music.play()
   
    #Задаем стартовые параметры игровым переменным
    max_speed=30 #Максимальная скорость
    speed=1 #Текущая скорость

    #Создаем словарь с данными о ракете
    rocket={'x':0,'y':300}

    #Переменные для фиксации нажатых клавиш
    #по умолчанию ничего не нажато
    keyup=False
    keydown=False
    keyspace=False

    done=False
    while done==False:
       # Обработка событий
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                done=True
            #Если пользователь нажал на кнопку
            if event.type == pygame.KEYDOWN:
                #Если нажата кнопка выхода (Escape)
                if event.key == pygame.K_ESCAPE:
                    done=True # то установить флаг для выхода
                #Если нажали кнопку вверх
                elif event.key == pygame.K_UP:
                    keyup=True # то выставляем флаг нажатия кнопки вверх
                elif event.key == pygame.K_DOWN:
                    keydown=True
                elif event.key == pygame.K_SPACE:
                    keyspace=True
            #Если пользователь Отпустил кнопку
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    keyup=False
                elif event.key == pygame.K_DOWN:
                    keydown=False
                elif event.key == pygame.K_SPACE:
                    keyspace=False
        #Обработка логики игры
        if keyup:
            rocket['y']-=10
            if rocket['y']<0:
                rocket['y']=0
        elif keydown:
            rocket['y']+=10
            if rocket['y']+images['rocket'].get_height()>screen.get_height():
                rocket['y']=screen.get_height()-images['rocket'].get_height()
        if keyspace:
            speed+=1
            if speed>max_speed:speed=max_speed
        else:
            speed-=1
            if speed<1:speed=1

        #Вывод на экран
            
        #Прокрутка справа налево
        buf.blit(images['fon'],[0,0],[speed,0,images['fon'].get_width(),images['fon'].get_height()])
        buf.blit(images['fon'],[images['fon'].get_width()-speed,0],[0,0,speed,images['fon'].get_height()])

        #Рисуем фон
        images['fon'].blit(buf,[0,0])

        screen.blit(images['fon'],[0,0])
        
        #Выводим картинку с ракетой
        screen.blit(images['rocket'],[rocket['x'],rocket['y']])

            
            
        pygame.display.flip()
        clock.tick(20)

    pygame.quit()

main()
